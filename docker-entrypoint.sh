#!/bin/sh

main () {
	if [ -z "$(ls -A /inbound 2>/dev/null)" ]; then
		echo "$(date +"%T") | No files found into inbound"
		exit 1
	else
		rsync -avz /inbound/ /outbound
	fi
}

main
