# PVC Sync

The purpose of this helm release is to copy a PVC to another in case if your PVC can not be resized.

Feel free to use it !
### Docker 
Docker repository : [Repository](https://hub.docker.com/repository/docker/panzouh/pvc-sync)

![Docker Image Size (tag)](https://img.shields.io/docker/image-size/panzouh/pvc-sync/alpha1)
![Docker Image Version (latest by date)](https://img.shields.io/docker/v/panzouh/pvc-sync)

## Installation
 Edit values.yaml 

```
helm install -f values.yaml -n my-namespace ./CHART_PATH
```

## Todo
- Create another volume to not shutdown app v2
- Write README on Docker repository

## License
License File : [License](LICENSE)
## Contribute
Comming soon !
## Authors
@Panzouh
