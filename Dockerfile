FROM alpine:3

RUN apk add --no-cache rsync

RUN mkdir /inbound && mkdir /outbound

ADD docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

VOLUME /inbound
VOLUME /outbound

ENTRYPOINT ["docker-entrypoint.sh"]
